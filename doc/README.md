<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# CommonMark Addon  
Integrates [CommonMark](https://commonmark.thephpleague.com/) with Liaison.   
  
This library is not affiliated with CommonMark.   
  
With Liaison, simply do:    
```php   
$cmark = new \Lia\Addon\CommonMark($liaison);    
# If you want any CommonMark extensions  
$cmark->enable_extension('table_of_contents', $configs[]);  
# for custom setup on the environment  
$cmark->add_hook('environment', function($environment){$environment->addRenderer(...); });  
```   
  
For a list of extensions, see [code/CommonMark.php](/code/CommonMark.php) `$extension_list` & `$default_configs` properties.   
  
**Note:** For extensions, you may need to add dependencies to your composer.json. CommonMark comes with quite a few built-in though.  
  
  
## Install  
```bash  
composer require taeluf/liaison.common-mark v0.3.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/liaison.common-mark": "v0.3.x-dev"}}  
```    
