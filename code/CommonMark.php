<?php


namespace Lia\Addon;

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkRenderer;
use League\CommonMark\MarkdownConverter;

class CommonMark {

    static public $disabled = false;

    /** commonmark instance */
    protected $cmark = null;
    /** commonmark environment instance */
    protected $environment = null;
    /**
     * list of enabled extensions
     * @key is numeric
     * @value is a string referencing a key in @property($extension_list)
     */
    protected $extensions = [];

    /**
     * callables to call at certain points of setup/running
     * @key is the name of the hook
     * @value is an array of callables
     */
    protected $hooks = [
        /** run after all extensions are added to the environment */
        'environment'=>[]
    ];

    static protected $instance = null;

    protected $extension_list = [
        'heading_permalink'=> 'League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension',
        'table_of_contents'=> 'League\CommonMark\Extension\TableOfContents\TableOfContentsExtension',
        'footnote'=> 'League\CommonMark\Extension\Footnote\FootnoteExtension'
    ];

    protected $extension_dependencies = [
        'table_of_contents'=>['heading_permalink'],
    ];

    protected $default_configs = 
        [
            'footnote'=>[
                'backref_class'      => 'footnote-backref',
                'backref_symbol'     => '↩',
                'container_add_hr'   => false, // changed from default of true
                'container_class'    => 'footnotes',
                'ref_class'          => 'footnote-ref',
                'ref_id_prefix'      => 'fnref:',
                'footnote_class'     => 'footnote',
                'footnote_id_prefix' => 'fn:',
            ],
            'table_of_contents'=>[
                'position'=>'placeholder',
                'placeholder'=>'[TOC]',
            ],
            'heading_permalink' => [
                'html_class' => 'heading-permalink',
                'id_prefix' => 'hh',
                'fragment_prefix' => 'hh',
                'insert' => 'before',
                'min_heading_level' => 1,
                'max_heading_level' => 2,
                'title' => 'Permalink',
                'symbol' => HeadingPermalinkRenderer::DEFAULT_SYMBOL .' ',
            ],
        ];

    /**
     *
     * @param $package a Liaison package
     */
    public function __construct(\Lia\Package $package, $config=null){
        if ($config!=null)$this->config = $config;

        $package->hook('RouteResolved', 
            [$this, 'route_resolved']
        );

        $package->addons['commonmark'] = $this;
    }

    public function add_hook($hook_name, $callable){
        $this->hooks[$hook_name][] = $callable;
    }

    /**
     * sets up internal config arrays to later add to commonmark
     */
    public function enable_extension($ext_name, $configs=[]){
        $options = array_merge($this->default_configs[$ext_name], $configs);
        $this->extensions[$ext_name] = $options;

        foreach ($this->extension_dependencies[$ext_name]??[] as $index=>$dependency){
            if (!isset($this->extensions[$dependency])){
                $this->enable_extension($dependency);
            }
        }
    }


    public function route_resolved($route, $response){

        if (static::$disabled)return;

        $url = $response->request->url();
        if (substr($url,-1)!='/')return;
        if (!$response->useTheme)return;

        $response->content = $this->convert_to_markdown($response->content);
    }

    public function convert_to_markdown(string $markdown){
        $cmark = $this->init_common_mark();
        $html = $markdown;

        // process only content inside <markdown> tags. If no <markdown> tags are present
        // then process the entire thing
        $hasMarkdownNode = strpos($html, '<markdown>') !== false;
        if (!$hasMarkdownNode)$html = '<markdown>'.$html.'</markdown>';

        while(($start=strpos($html, '<markdown>'))!==false){
            $tagLen = strlen('<markdown>');
            $mdStart = $start + $tagLen;
            $end = strpos($html, '</markdown>', $start);
            $md = substr($html, $mdStart, $end-$mdStart);

            $convertedToHtml = $cmark->convertToHtml($md);
            $html= substr($html, 0,$start)
                . $convertedToHtml
                . substr($html, $end + strlen('</markdown>'));
        }
        return $html;
    }


    public function init_environment(){
        if ($this->environment!=null)return $this->environment;
        $environment = new Environment($this->extensions);
        $environment->addExtension(new CommonMarkCoreExtension());

        foreach ($this->extensions as $key=>$options){
            $ext_class = $this->extension_list[$key] ?? null;
            if ($ext_class==null)continue;
            $ext = new $ext_class();
            $environment->addExtension($ext);
        }

        foreach ($this->hooks['environment'] as $callable){
            $callable($environment);
        }
        $this->environment = $environment;
        return $environment;
    }

    public function init_common_mark(){
        if ($this->cmark!=null)return $this->cmark;
        $environment = $this->init_environment();

        $cmark = new MarkdownConverter($environment);
        $this->cmark = $cmark;
        return $cmark;
    }
}

