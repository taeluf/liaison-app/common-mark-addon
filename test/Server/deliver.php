<?php

require_once(dirname(__DIR__,2).'/vendor/autoload.php');

$lia = new \Lia();
$server = new \Lia\Package\Server($lia);
$main = new \Lia\Package\Server($lia, 'main', __DIR__);
$cmark = new \Lia\Addon\CommonMark($main);

$lia->deliver();
