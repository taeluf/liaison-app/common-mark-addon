<?php

namespace Lia\Addon\CommonMark\Test;

class Main extends \Tlf\Tester {

    public function prepare(){

    }

    public function testHighlight(){
        $lia = new \Lia();
        $server = new \Lia\Package\Server($lia);
        $cmark = new \Lia\Addon\CommonMark($server);
        $content = $cmark->convert_to_markdown(file_get_contents($this->dir().'Header_P.md'));

        $this->str_contains($content, '<h1>Test</h1>');
        $this->str_contains($content, '<p>Test paragraph</p>');
    }


    public function dir(){
        return $this->cli->pwd.'/test/input/';
    }

}
