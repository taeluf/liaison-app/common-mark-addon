<?php

namespace Lia\Addon\CommonMark\Test;

class Server extends \Tlf\Tester {

    public function prepare(){

    }

    public function testIntegration(){
        $content = $this->get('/');

        echo "Response:\n\n";
        echo $content;

        $this->str_contains($content, '<h1>Test</h1>');
        $this->str_contains($content, '<p>Test paragraph</p>');
    }

}
