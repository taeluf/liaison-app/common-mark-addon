# Dev Status

## Dec 10, 2021 end of day
- this needs massive simplification. See the mdblog addon for notes & inspiration

## Versions
- v0.2: liaison v0.3 (but not in the composer!), scrawl v0.5
- v0.3: liaison v0.5, scrawl v0.6

# Dec 10, 2021
- DONE get working with new liaison
- DONE add test for basic md conversion
- TODO (eventually) additional tests for robustness.
- CONSIDER it's not a subclss of Addon ... maybe it should be


## changes in v0.2
- rewriting the commonmark extension main class a little bit to make it ... less confusing? more robust?
